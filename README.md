# README

## How do I view a presentation?

Simply drag the presentation you wish to view (e.g. ch1.html) from a Finder window or a text editor into your favorite browser window.
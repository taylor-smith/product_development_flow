class: center, middle

# Principles of Product Development Flow: Chapter 1
## Taylor Smith
## December 2, 2016

---
class: center, middle

##1. Why?
##2. What Is The Problem?
##3. Problems with Current Orthodoxy
##4. Possible Solutions and Themes
##5. Discussion

---
class: center, middle

# Why?

???

* Dominant paradigm for managing product development is fundamentally wrong

---

# Consider two dominant paradigms in product development

###* Phase gate development
###* Lean manufacturing (flow)

???
* Phase gate development--work moves from one phase to another and cannot move on until one phase is completed. Product Requirements -> Design -> Development
* This tends to be hard to work with so teams find ways around it. We find that teams actually begin phases before the previous one is finished e.g. requirements gathering and design. Average product developer begins design when 50% of product requirements are known
* Goal to achieve flow, like lean manufacturing. However some key differences. Manufacturing tasks are predictable and repetitive, with homogenous delay costs, homogeneous task durations. Best accomplished in FIFO
* Product development deals with high variability, non repetitive, non homogeneous tasks. Different projects have different delay costs and present different load on resources

---
class: center, middle

# What is the problem?

## Self-reinforcing, dysfunctional system

???
* I say self-reinforcing because the beliefs are internally consistent and tightly interlocked
* examples
* "Efficiency is good" w/ blindnness to queues
* Load dev processes to high levels of capacity (underutilized resources represent waste), which causes queues, and queues have no apparent cost. We will see that this leads to large queues and long cycle times.
* "Variability is bad" w/ failure to correctly quantify economics
* Minimize variability by sacrificing other unquantified economic goals ("do it right the first time"). Risk-averse product development kills innovation

---
class: center, middle
# Problems with Current Orthodoxy

???
* These are well-defended ideas, so some of these suggestions are controversial

---
class: center, middle

![Figure 1-1](Figure 1-1.png)

Reinertsen, *The Principles of Product Development Flow: Second Edition*, 2009, Chapter 1, The Principles of Flow

???
* Failure to Correctly Quantify Economics: Single biggest weakness. Developers focus on proxy variables--a proxy variable is a quanitifed measure that substititutes for real economic objective, *life cycle profits*. E.g. cycle time (example: measure cycle time but do not know what it costs to delay a week). Proxy variables influence each other (e.g. capacity utilization and cycle times), so moving the needle one way causes side effects. The only unit of measure that does not have side effects is life-cycle profits.
* Blindness to Queues: Queues are single most important cause of poor product development because they cause Design In Process inventory--as soon as a piece of work hits our process, it is Design In process. In product development, our inventory is invisible financially and physically. DIP increases variability, risk, and cycle time. Decreases efficiency, quality, and motivation. To understand the economic cost of queues, answer two questions: how big are our queues? What is the cost of these queues (i.e. what is the cost of delay?)? This leads back to correctly quanitifying economics.
* Institutionalization of Large Batch Sizes: appear to reduce variability because they pool many activities together, but we fail to recognize the relationship between batch size and cycle time, and batch size and feedback speed. We are going to see that limiting batch size is the single most cost-effective way for reducing queues because they reduce unnecessary variability in flow
* Managing timeline instead of queues: we don't understand statistics of variability, so we create a granular timeline to subdivide intervals into very small buckets. *Variance is high because coefficient of variation for each of those buckets is high*. Incentivizing conformance to timeline encourages people to insert contingency to avoid missing schedules. Cyclical problem--longer timelines lead to more contingency, which leads to longer timelines. We want to manage queues instead of cycle times because we will see that queues are a leading indicator of cycle times--by controlling queues, we automatically control timelines.

---
class: center, middle

# Possible Solutions and Themes

---
class: center, middle

![Figure 1-2](Figure 1-2.png)

Reinertsen, *The Principles of Product Development Flow: Second Edition*, 2009, Chapter 1, The Principles of Flow

???
* Economics is ultimate foundation of the book. Current paradigm observes correlation between proxy variables
* Queues and Queuing Theory--understand the importance of queues and how to manage them
* Variability--variability can be good if it improves economic payoff
* Batch Size--once we recognize queues are the problem, we are led to finding methods to reduce queues. We will discover that reducing batch size is the single most cost-effective way to reduce queues.
* WIP Constraints exploit direct relationship between cycle time and inventory (Little's Formula). Kanban
* Cadence, Synchronization, and Flow Control--cadence = processing work at regular time intervals. Synchronization = making multiple tasks start or finish at the same time. Flow control = sequence in which we process jobs
* Fast feedback--we rely on feedback to influence subsequent choices
* Our examples for these will come from: lean manufacturing, economics, queuing theory, statistics, the internet and telecom, and OS design

---
class: center, middle
# Discussion

???
* Why is worship of efficiency detrimental? (page 6)
* What about hostility to variability? (page 8)
* Noneconomic control flow--prioritization on ROI. Is this a real economic indicator? Why or why not? (page 13)
* Consider two projects: a low cost of delay project with a small (depleted) buffer (not much time left), or a high cost of delay project that is ahead of schedule. Which should the business deploy resources to? (In general it is best to delay the project with a low cost of delay)
* In your experience, does centralized control improve or diminish feedback? What about decentralized control?
* Discuss the value of feedback with regards to Figure 1-3 (page 19)
* Compare the feedback of inventory levels in a factory to that of a product development team.